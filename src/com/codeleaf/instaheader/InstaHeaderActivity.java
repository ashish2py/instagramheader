package com.codeleaf.instaheader;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class InstaHeaderActivity extends Activity implements AbsListView.OnScrollListener{

    ListView list; 
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        list = (ListView) findViewById(R.id.list);
        list.setAdapter(new Adapter(this));        
        list.setOnScrollListener(this); 
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem,
            int visibleItemCount, int totalItemCount) {     
        //the listview has only few children (of course according to the height of each child) who are visible
        for(int i=0; i < list.getChildCount(); i++){
            View child = list.getChildAt(i);
            ViewHolder holder = (ViewHolder) child.getTag();

            //if the view is the first item at the top we will do some processing
            if(i == 0){             
                boolean isAtBottom = child.getHeight() <= holder.header.getBottom();
                int offset = holder.previousTop - child.getTop();
                if(!(isAtBottom && offset > 0)){                    
                    holder.previousTop = child.getTop();
                    holder.header.offsetTopAndBottom(offset);                   
                    holder.header.invalidate();
                }
            } //if the view is not the first item it "may" need some correction because of view re-use
            else if (holder.header.getTop() != 0) {
                int offset = -1 * holder.header.getTop(); 
                holder.header.offsetTopAndBottom(offset);
                holder.previousTop = 0;
                holder.header.invalidate();
            }
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {}

    public class Adapter extends ArrayAdapter<String> {
        public Adapter(Context context) {
            super(context, R.layout.row, R.id.header);
            for(int i=0; i < 50; i++){
                add(Integer.toString(i));
            }
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView == null){
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.row, parent, false);
                ViewHolder holder = new ViewHolder();
                holder.header = (TextView) convertView.findViewById(R.id.header);
                convertView.setTag(holder);             
            }
            ViewHolder holder = (ViewHolder) convertView.getTag();
            holder.header.setText(getItem(position));
            
            //callanother listview
            ListView childList = (ListView) convertView.findViewById(R.id.childList);
            ChildAdapter adapter = new ChildAdapter(InstaHeaderActivity.this);
            childList.setAdapter(adapter);
            
            return convertView;
        }
    }


    //child header adapter
    public class ChildAdapter extends ArrayAdapter<String> {
        public ChildAdapter(Context context) {
            super(context, R.layout.child_header, R.id.childHeader);
            for(int x=0; x< 6; x++){
                add(Integer.toString(x));
            }
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView == null){
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.child_header, parent, false);
                ViewHolder holder = new ViewHolder();
                holder.header = (TextView) convertView.findViewById(R.id.childHeader);
                convertView.setTag(holder);             
            }
            ViewHolder holder = (ViewHolder) convertView.getTag();
            holder.header.setText(getItem(position));
            return convertView;
        }
    }


    
    public class ViewHolder {
        TextView header;
        int previousTop = 0;
    }
}